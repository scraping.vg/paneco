import unittest
import common

class TestCommon(unittest.TestCase):
    def test_extract_vol(self):
        s = '700 מ״ל'
        res = common.extract_vol(s)
        self.assertEqual(700.0, res)
        s = '1 ליטר'
        res = common.extract_vol(s)
        self.assertEqual(1000.0, res)
        s = '1.2 ל'
        res = common.extract_vol(s)
        self.assertEqual(1200.0, res)
        s = '1000'
        res = common.extract_vol(s)
        self.assertEqual(1000.0, res)
        s = '750מ״ל'
        res = common.extract_vol(s)
        self.assertEqual(750.0, res)

    def test_extract_abv(self):
        s = '53%'
        res = common.extract_abv(s)
        self.assertEqual(53.0, res)
        s = '54 %'
        res = common.extract_abv(s)
        self.assertEqual(54.0, res)
        s = '55.6 %'
        res = common.extract_abv(s)
        self.assertEqual(55.6, res)

if __name__ == '__main__':
    unittest.main()