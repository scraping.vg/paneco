from openai import OpenAI 
import os
from dotenv import load_dotenv

def ai_analyze(image):
	MODEL="gpt-4o"
	client = OpenAI(api_key=os.environ.get("OPENAI_API_KEY"))
	load_dotenv()
	response = client.chat.completions.create(
		model=MODEL,
		messages=[
			{"role": "system", "content": "You are an assistant that provides the full answer in a comma separated form"},
			{"role": "user", "content": [
				{"type": "text", "text": "What's the whisky name, years of maturation if specified, cask details if present, edition name if present, is peated / smoky or not, and the distillery of the product on the picture"},
				{"type": "image_url", "image_url": {
					"url": f"data:image/png;base64,{image}"}
				}
			]},
		],
		temperature=0.0,
	)
	return response.choices[0].message.content
# print(response.choices[0].message.content)

# Glencadam, 13 years, Batch 3 Limited Edition, Glencadam Distillery
