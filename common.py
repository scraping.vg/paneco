import re

def extract_vol(input: str)-> float:
	try:
		return float(input)
	except:
		pass
	if "מ" in input or 'm' in input:
		multiplier = 1
	else: 
		multiplier = 1000

	fields = re.sub('[a-zA-Z״א-ת]+', ' ', input).strip().split(' ')
	for item in fields:
		try:
			return float(item) * multiplier
		except:
			continue
	return 0.0


def extract_abv(input: str) -> float:
	fields = input.strip().replace('%', '').split(' ')
	for item in fields:
		try:
			return float(item)
		except:
			continue
	return 0.0