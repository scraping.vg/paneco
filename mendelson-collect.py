import requests
from bs4 import BeautifulSoup
from unidecode import unidecode
import base64
import csv
import common
import ai


list_of_cat = ['https://mendelson-heshin.com/product-category/וויסקי/page/',]

links = []
for cat in list_of_cat:
	for num in range(50):
		response = requests.get(f'{list_of_cat[0]}/{num+1}/')
		if 'products' in str(response.text):
			bea = BeautifulSoup(response.text, 'html.parser')
			products = bea.select('.products')
			for item in products:
				for li in item.findAll('li'):
					link = ''
					link = li.select('a')[0].attrs.get('href', '')
					links.append(link)
		else:
			print("done collecting links at {num}")
			break
whiskies = []
for link in links:
	whisky = {
		'english name': '',
		'bottle volume': '',
		'abv': '',
		'hebrew name': '',
		'ai name': '', 
		'ai years of maturation': '',
		'ai cask details': '',
		'ai edition name': '',
		'ai peated': '',
		'ai distillery': '',
		'link': link,
	}
	response = requests.get(link)
	bea = BeautifulSoup(response.text, 'html.parser')
	title = bea.select('.product_title')[0].text.strip().replace('-', '--').replace("–", "--")
	title = [i.strip() for i in title.split("--")]
	whisky['hebrew name'] = title[0]
	if  len(title) > 1:
		whisky['english name'] = title[1]
	try:	
		for att in bea.select('.attribute_wrapper').pop():
		
			args =att.text.split('\n')
			for i in range(len(args)-1):
				if args[i] == 'אחוז אלכוהול':
					whisky['abv'] = common.extract_abv(args[i+1])
					continue
				if args[i] == "כמות":
					whisky['bottle volume'] = common.extract_vol(args[i+1])
					continue
	except Exception as e:
		print(att, e, link)
		continue
	if whisky.get("abv") == '':
		continue
	if whisky.get("bottle volume") == '':
		continue
	try:
		pic = bea.select('.woocommerce-product-gallery__image')
		image_url = pic[0].select('a')[0].attrs.get('href', '')
		img_data = requests.get(image_url).content
		image_b64 = base64.b64encode(img_data).decode("utf-8")
		r = ai.ai_analyze(image_b64).split(',')
		r = [i.strip() for i in r]
		if len(r) == 6:
			whisky['ai name'] = r[0]
			whisky['ai years of maturation'] = r[1]
			whisky['ai cask details'] = r[2]
			whisky['ai edition name'] = r[3]
			whisky['ai peated'] = r[4]
			whisky['ai distillery'] = r[5]
	except Exception as e:
		print(e, " pic ", link)
		pic = ''
	whiskies.append(whisky)
	print(".", end =" ", flush=True)
	
with open('collection-mendelson.csv','w') as f:
    w = csv.writer(f)
    w.writerow(whiskies[0].keys())
    for whisky in whiskies:
        w.writerow(whisky.values())
