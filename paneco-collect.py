import requests
from bs4 import BeautifulSoup
from unidecode import unidecode
import base64
import csv
import common
import ai

list_of_cat = ['https://www.paneco.co.il/whiskey?',
			#   'https://www.paneco.co.il/alcohol?',
			#   'https://www.paneco.co.il/wine?',
			#   'https://www.paneco.co.il/בירה-2?',
			#    'https://www.paneco.co.il/משקאות-אנרגיה?',
			#    'https://www.paneco.co.il/pack?',
			#    'https://www.paneco.co.il/מתנות-ומארזים',
			#    'https://eilat.paneco.co.il/'
			]

links = []
for items in list_of_cat:
	# for num in range(100):
	for num in range(50):
		if 'item product product-item' in str(requests.get(f'{list_of_cat[0]}p={num+1}').text):
			response = requests.get(f'{items}p={num + 1}')
			bea = BeautifulSoup(response.text, 'html.parser')
			iter = bea.select('.product-item .product-item-details')
			
			for item in iter:
				try:
					link = ''
					product = item.select('.product a')
					link = product[0].attrs.get('href', '')
					links.append(link)
					#break ## remove

				except Exception as e:
					pass
					
		else:
			print("done list of links")
			break
whiskies = []

for link in links:
	whisky = {
		'english name': '',
		'bottle volume': '',
		'abv': '',
		'hebrew name': '',
		'ai name': '', 
		'ai years of maturation': '',
		'ai cask details': '',
		'ai edition name': '',
		'ai peated': '',
		'ai distillery': '',
		'link': link,
	}
	response = requests.get(link)
	bea = BeautifulSoup(response.text, 'html.parser')
	title = bea.select('.page-title .base')[0].text.strip()
	whisky['hebrew name'] = title
	# print(title)
	p = bea.select('.product-info-price .price-container .price-wrapper .price')[0].getText(strip=True).replace(',', '')
	price = float(unidecode(p).split(' ')[0])
	
	try:
		p = bea.select('.special-price .price-wrapper .price')[0].getText(strip=True).replace(',', '')
		special_price = float(unidecode(p).split(' ')[0])
	except Exception:
		special_price = price
	# print(price, special_price)
	try:
		english_title = bea.select('.product-english-title')[0].getText(strip=True)
		# print(english_title)
	except Exception as e:
		english_title = ''
		print("can't find english title for {link}: {e}")
	whisky['english name'] = english_title
	addit = bea.find(attrs={'class':'additional-attributes'})
	table_body = addit.find('tbody')
	rows = table_body.find_all('tr')
	data = {}
	for row in rows:
		v = row.find_all('td')[0].text
		k = row.find_all('th')[0].text
		data[k] = v
		
	
	whisky['bottle volume'] = common.extract_vol(data.get('נפח הבקבוק', ''))
	whisky['abv'] = common.extract_abv(data.get('אחוז אלכוהול', ''))
	# print("additional data: ", data)
	try:
		canbuy = bea.select('.product-info-main .product-add-form .box-tocart .actions .tocart')[0].getText(strip=True)
		print(".", end =" ", flush=True)
		# print("In stock: ", canbuy == 'הוסף לסל')
	except:
		canbuy = ''
		print("-", end =" ", flush=True)
	if canbuy != 'הוסף לסל':
		# print("Skipping oit of stock item: ", whisky)
		continue
	try:
		pic = bea.select('.gallery-placeholder .gallery-placeholder__image')
		image_url = pic[0]["src"]
		img_data = requests.get(image_url).content
		image_b64 = base64.b64encode(img_data).decode("utf-8")
		r = ai.ai_analyze(image_b64).split(',')
		r = [i.strip() for i in r]
		if len(r) == 6:
			whisky['ai name'] = r[0]
			whisky['ai years of maturation'] = r[1]
			whisky['ai cask details'] = r[2]
			whisky['ai edition name'] = r[3]
			whisky['ai peated'] = r[4]
			whisky['ai distillery'] = r[5]
	except Exception as e:
		pic = ''
		print(e)
	whiskies.append(whisky)
	
with open('collection-paneco.csv','w') as f:
    w = csv.writer(f)
    w.writerow(whiskies[0].keys())
    for whisky in whiskies:
        w.writerow(whisky.values())



